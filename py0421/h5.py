import random
def qs(a):
    if len(a)<=1:
        return a
    left=[]
    right=[]
    center=a.pop(random.randint(0,len(a)-1))
    for item in a:
        if item < center:
            left.append(item)
        else:
            right.append(item)
    return qs(left)+[center]+qs(right)

print(qs([2,1,3,6,5,4]))
