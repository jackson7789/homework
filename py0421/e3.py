import sys
days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday']
num = int(sys.argv[1])
if num == 7:
    print(days[0])
elif (num > 7 or num <= 0):
    print('Unknown')
else:
    print(days[num])
