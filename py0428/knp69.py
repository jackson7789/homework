#coding: utf-8
from pyknp import KNP
import sys
knp = KNP()
data = ''
for line in iter(sys.stdin.readline, ''):
    data += line
    if line.strip() == 'EOS':
        result = knp.result(data)
        for bnst in result.bnst_list():
            for mrph in bnst.mrph_list():
                if mrph.hinsi == '接頭辞':
                    print("".join(mrph.midasi for mrph in bnst.mrph_list()))
                    break
        data = ''
