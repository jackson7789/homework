#!/usr/bin/env zsh

web_dir=$1
log_dir_name=$2

function usage {
  echo 'Usage: ./web_log.sh web_dir log_dir'
  echo 'e.g. ./web_log.sh /loquat/chu/ems/ASPEC-CJ/shen-parse/CTB5-NICT-short/nile/web cj-short'
  exit -1
}

if [ $# -ne 2 ]; then
  usage
fi

for file in `ls $web_dir/*.web`; do
    base=`expr $file : ".*/\(.*\)\.web"`
    echo "/share/usr-x86_64/bin/perl ~/public_html/webmt/mt/cgi-bin/dump2web.pl $log_dir_name $base < $web_dir/$base < $web_dir/$base.tm < $web_dir/$base.web"
    /share/usr-x86_64/bin/perl ~/public_html/webmt/mt/cgi-bin/dump2web.pl $log_dir_name $base < $web_dir/$base < $web_dir/$base.tm < $web_dir/$base.web
done
