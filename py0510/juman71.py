#coding: utf-8
from pyknp import KNP
import sys
knp = KNP()
data = ''
for line in iter(sys.stdin.readline, ''):
    data += line
    if line.strip() == 'EOS':
        result = knp.result(data)
        for bnst in result.bnst_list():
            for i in range(len(bnst.mrph_list())-1):
                if "<自立>" in bnst.mrph_list()[i].fstring:
                    if "<自立>" in bnst.mrph_list()[i+1].fstring:
                        print(bnst.mrph_list()[i].midasi,bnst.mrph_list()[i+1].midasi,sep="")
        data = ""
