#coding: utf-8
from pyknp import KNP
import sys
knp = KNP()
data = ''
for line in iter(sys.stdin.readline, ''):
    data += line
    if line.strip() == 'EOS':
        result = knp.result(data)
        for bnst in result.bnst_list():
            parent = bnst.parent
            for mrph in bnst.mrph_list():
                print(mrph.repname)
