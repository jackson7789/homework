import math
#1
print('1. 1から30までの整数を出力せよ.')
for i in range(1, 31):
    print(i)
#2
print('\n2. 1から30までに現れる偶数を出力せよ.')
for i in range(2, 31, 2):
    print(i)
#3
print('\n3. 1から30までに現れる偶数の和を計算せよ.')
asum = 0
for i in range(2, 31, 2):
    asum += i
print(asum)
#4
print('\n4. 10! (10の階乗) を計算せよ.')
result = 1
for i in range(1, 11):
    result *= i
print(result)
#5
print('\n5. 九九の表を作成せよ.')
for i in range(1,10):
    for j in range(1,i+1):
        print(j,'*',i,'=','{0:2d}'.format(i*j),'  ',sep='',end='')
    print('')
#6
print('\n6. Fizz and Buzz')
fizz = []
buzz = []
fizzbuzz = []
count = 0
for i in range(1,31):
    if i % 3 == 0:
        if i % 5 == 0:
            fizzbuzz.append(i)
        else :
            fizz.append(i)
    elif i % 5 == 0:
        buzz.append(i)
    else:
        count += 1
print('{0:8}'.format('Fizz'),fizz,sep=' :')
print('{0:8}'.format('Buzz'),buzz,sep=' :')
print('FizzBuzz', fizzbuzz,sep=' :')
print('{0:8}'.format('Others'),count,sep=' :')
#7
print('\n7. 1000までの素数を全て出力せよ.')
prime = list(range(2,1001))
for i in range(2,1001):
    for j in range(2,int(math.sqrt(i))+1):
        if i%j == 0:
            prime.remove(i)
            break
print(prime)
