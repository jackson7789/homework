from pyknp import Juman
import sys
j = Juman()
judge = ['動詞','形容詞']
sentence = ''
count = 0
total = 0
for line in iter(sys.stdin.readline, ""):
    sentence += line
    if line.strip() == "EOS":
        flag = False
        result = j.result(sentence)
        total += len(result.mrph_list())
        for mrph in result.mrph_list():
            if mrph.hinsi in judge:
                count += 1
        sentence = ''
print(count/total)
