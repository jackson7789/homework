from pyknp import Juman
import sys
j = Juman()
sentence = ''
for line in iter(sys.stdin.readline, ""):
    sentence += line
    if line.strip() == "EOS":
        result = j.result(sentence)
        result = result.mrph_list()
        for i in range(len(result)):
            if result[i].midasi == 'の':
                if i == 0:
                    continue
                if result[i-1].hinsi == '名詞' and result[i+1].hinsi == '名詞':
                    print(result[i-1].midasi,result[i].midasi,result[i+1].midasi,sep = '')
        sentence = ''
