from pyknp import Juman
import sys
j = Juman()
judge = ['する','できる','出来る']
sentence = ''
for line in iter(sys.stdin.readline, ""):
    sentence += line
    if line.strip() == "EOS":
        result = j.result(sentence)
        result = result.mrph_list()
        for i in range(len(result)):
            if result[i].bunrui == 'サ変名詞':
                if result[i+1].genkei in judge:
                    print(result[i].midasi, result[i+1].genkei, sep = '')
        sentence = ''
