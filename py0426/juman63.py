#coding: utf-8
from pyknp import Juman
import sys
j = Juman()
flag = False
sentence = ''
for line in iter(sys.stdin.readline, ""):
    sentence += line
    if line.strip() == "EOS":
        flag = False
        result = j.result(sentence)
        for mrph in result.mrph_list():
            if mrph.hinsi == '動詞':
                flag = True
                print(mrph.genkei, sep=' ', end = ' ')
        sentence = ''
        if flag:
            print('')
        else:
            print('None')
    
