from pyknp import Juman
import sys
j = Juman()
wdict = {}
sentence = ''
for line in iter(sys.stdin.readline, ""):
    sentence += line
    if line.strip() == "EOS":
        flag = False
        result = j.result(sentence)
        for mrph in result.mrph_list():
            if mrph.genkei in wdict:
                wdict[mrph.genkei] +=1
            else:
                wdict[mrph.genkei] = 1
        sentence = ''
#print(wdict)
#print(iter(wdict.items()))
sdict = sorted(iter(wdict.items()), key=lambda asd:asd[1] ,reverse = True)
#print(sdict)
for item in sdict:
    print(item[0],':',item[1],'回',sep = '')
