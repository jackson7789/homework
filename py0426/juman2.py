#coding: utf-8
from pyknp import Juman
import sys
juman = Juman()
data = ''
for line in iter(sys.stdin.readline, ""):
    data += line
    if line.strip() == "EOS":
        result = juman.result(data)
        for w in result.mrph_list():
            if w.hinsi == '名詞':
                print(w.midasi , end=' ')
        data = ""
        print('')
