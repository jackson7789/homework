#coding: utf-8
import re
import string

def cutEnd(s):
    #
    while re.compile("\.$|\?$|\!$|\,$|\'$").search(s):
        s = s[:-1]
    while re.compile('^\'').search(s):
        s = s[1:]
    return s

def delPunc(s):
    #
    deller = re.compile('[%s]' % re.escape(string.punctuation))
    return deller.sub('',s)

s = 'The man is in the house.'
slist = s.strip('.').lower().split(' ')
bilist = []
wordsNum = 0

#
for i in range(len(slist)):
    if i<len(slist)-1:
        a = slist[i]+' '+slist[i+1]
        bilist.append(a)

#
sdict = {}
for item in slist:
    sdict[item] = 0
for item in bilist:
    sdict[item] = 0

#
with open('~/doc.txt') as txt:
    for line in txt:
        if re.match(r'<PAGE.*?>', line) == None and re.match(r'</PAGE.*?>', line) == None:
            line = line.strip().lower()
            biwords = []
            temp = line.split(' ')
            for i in range(len(temp)):
                if i < len(temp)-1:
                    bi = temp[i] + ' ' + temp[i+1]
                    biwords.append(bi)
            for biword in biwords:
                if cutEnd(biword) in bilist:
                    sdict[cutEnd(biword)] += 1
            words = delPunc(line).split(' ')
            wordsNum += len(words)
            for word in words:
                if word in slist:
                    sdict[word] += 1

#
p = 1
for i in range(len(slist)):
    if i == 0:
        p = p * sdict[slist[i]]/wordsNum
        biw = slist[i]+' '+slist[i+1]
        p = p * (sdict[biw] / sdict[slist[i]])
    elif i != len(slist)-1:
        biw = slist[i]+' '+slist[i+1]
        p = p * (sdict[biw] / sdict[slist[i]])
print(p)
